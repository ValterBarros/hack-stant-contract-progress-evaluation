require_relative 'ex'
class Use
  include ::Ex

  def initialize(id)
    @id = id
  end

  def perform
    execute
  end

  private

  attr_reader :id

  def execute
    puts id
  end
end
